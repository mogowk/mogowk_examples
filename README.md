# Some examples of the mogowk lib

* [Hello World](https://gitlab.com/mogowk/mogowk_examples/-/blob/master/01_hello_world/main.go)
* [Slices](https://gitlab.com/mogowk/mogowk_examples/-/blob/master/02_slices/main.go) (events, input value)

## Run examples

**Install local wasm http server**

`go get -u github.com/hajimehoshi/wasmserve`

**Run it in the example directory**

`cd ./hello_world`

`wasmserve`

**Open** [http://localhost:8080/](http://localhost:8080/)
