package main

import (
	"gitlab.com/mogowk/mogowk"
	"gitlab.com/mogowk/mogowk/tag"
)

// Page is your custom component
type Page struct {
	mogowk.Component // must to be embedded

	text string
}

// Render implements of the mogowk.Renderer interface
// This is rander of your component
func (p *Page) Render() *mogowk.Tag {
	return tag.Body().WithChilds(
		tag.H1().WithChilds(tag.Text(p.text)),
	)
}

func main() {
	mogowk.SetTitle("Mogowk")
	p := &Page{text: "Hello mogowk"}
	mogowk.RenderBody(p)
}
