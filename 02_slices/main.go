package main

import (
	"syscall/js"

	"gitlab.com/mogowk/mogowk"
	"gitlab.com/mogowk/mogowk/event"
	"gitlab.com/mogowk/mogowk/tag"
)

// Item of the slice
type Item struct {
	mogowk.Component

	Name string
}

// Render renders element <li>text</li>
func (i *Item) Render() *mogowk.Tag {
	return tag.LI().WithChilds(tag.Text(i.Name))
}

// Lister component with list of items
type Lister struct {
	mogowk.Component

	List    []*Item
	PopElem string

	pushInput js.Value
}

// onCilckPushElem adds new element from input tag
func (l *Lister) onCilckPushElem(_ *mogowk.EventData) {
	newItem := &Item{Name: l.pushInput.Get("value").String()}
	l.List = append(l.List, newItem)

	l.pushInput = js.Undefined() // сlean value

	mogowk.Rerender(l)
}

// onCilckPopElem extract first element from list and put it into PopElem
func (l *Lister) onCilckPopElem(_ *mogowk.EventData) {
	defer mogowk.Rerender(l) // Rerender Lister

	if len(l.List) == 0 {
		l.PopElem = "List is empty"
		return
	}

	// Pop
	l.PopElem, l.List = l.List[0].Name, l.List[1:]
}

// Render render list, pop elemnt and contorl elements
func (l *Lister) Render() *mogowk.Tag {
	inputTag := tag.Input()
	if !l.pushInput.IsUndefined() { // set old value
		inputTag.WithValue(l.pushInput.Get("value").String())
	}
	l.pushInput = inputTag.CompileElement()

	return tag.Div().WithChilds(
		inputTag,
		tag.Button().
			WithEvents(event.Click(l.onCilckPushElem).PreventDefault()).
			WithChilds(tag.Text("push elemnt")),

		tag.BR(),
		tag.BR(),

		tag.Button().
			WithEvents(event.Click(l.onCilckPopElem).PreventDefault()).
			WithChilds(tag.Text("pop front")),

		tag.BR(),
		tag.Span().WithChilds(tag.Text("pop element: "+l.PopElem)),
		tag.BR(),
		tag.Span().WithChilds(tag.Text("List: ")),
		tag.UL().WithListChild(l.List),
		tag.BR(),
	)
}

// Page isa main component with body tag
type Page struct {
	mogowk.Component

	Lister *Lister
}

// Render renders body tag and lister component
func (p *Page) Render() *mogowk.Tag {
	return tag.Body().WithChilds(
		p.Lister,
	)
}

func main() {
	p := &Page{
		Lister: &Lister{
			List:    []*Item{&Item{Name: "one"}, &Item{Name: "two"}, &Item{Name: "three"}},
			PopElem: "",
		},
	}

	mogowk.RenderBody(p)
}
